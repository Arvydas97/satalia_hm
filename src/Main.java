import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {

        getValues();

    }
    private static void getValues() throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));

        // Reading data using readLine
        System.out.println("Enter name, last name, field of studies, your score, level of study");
        System.out.println("Please separate it with commas.");
        String line = reader.readLine();

        createStudent(line.split(","));
    }

    private static void createStudent(String[] arrayOfSplits) throws IOException {
        switch (arrayOfSplits[4]) {
            case "Bachelor":
            case "bachelor":
                Student b = new Bachelor(arrayOfSplits);
                System.out.println(b.getScholarshipConclusion());
                break;
            case "Master":
            case "master":
                Student m = new Master(arrayOfSplits);
                System.out.println(m.getScholarshipConclusion());
                break;
            case "Phd":
            case "phd":
                Student p = new Phd(arrayOfSplits);
                String a =p.name;
                System.out.println(p.getScholarshipConclusion());
                break;
            default:
                getValues();
        }
    }
}