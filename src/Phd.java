public class Phd extends Student{
    protected String studiesLevel;
    protected double scholarship;

    public String getStudiesLevel() {
        return studiesLevel;
    }
    private double getScholarship(){
        return scholarship;
    }
    public String getScholarshipConclusion() {
        return toString() + " gaus " + getScholarship() + " € stipendiją";
    }

    public void setScholarship() {
        scholarship = 800;
    }
    Phd(String[] items) {
        super(items);
        this.studiesLevel = items[4];
        setScholarship();
    }

    public String toString() {
        return "Dr. " + super.toString();
    }
}
