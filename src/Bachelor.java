public class Bachelor extends Student {
    protected String studiesLevel;
    protected double scholarship;

    public String getStudiesLevel() {
        return studiesLevel;
    }
    private double getScholarship(){
        return scholarship;
    }
    public String getScholarshipConclusion() {
        return super.toString() + " gaus " + getScholarship() + " € stipendiją";
    }

    public void setScholarship() {
        if (score>8)
            scholarship = 100;
        else
            scholarship = 0;
    }
    Bachelor(String[] items) {
        super(items);
        this.studiesLevel = items[4];
        setScholarship();
    }

}
