public class Master extends Student {
    protected String studiesLevel;
    protected double scholarship;

    public String getStudiesLevel() {
        return studiesLevel;
    }
    private double getScholarship(){
        return scholarship;
    }

    public String getScholarshipConclusion() {
        return super.toString() + " gaus " + getScholarship() + " € stipendiją";
    }

    public void setScholarship() {
        if (studies.matches("math|physics|it"))
            scholarship = 200;
        else
            scholarship = 0;
    }

    Master(String[] items) {
        super(items);
        this.studiesLevel = items[4];
        setScholarship();
    }
}
