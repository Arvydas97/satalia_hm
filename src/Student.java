public abstract class Student implements StudentInterface {

    protected final String name;
    protected final String lastName;
    protected final String studies;
    protected final double score;


    protected Student(String name, String lastName,
                   String studies, double score){
        this.name = name;
        this.lastName = lastName;
        this.studies = studies;
        this.score = score;
    }

    Student(String[] items){
        this.name = items[0];
        this.lastName = items[1];
        this.studies = items[2];
        this.score = Double.parseDouble(items[3]);
    }

    public abstract void setScholarship();

    @Override
    public String toString() {
        return this.name + " " +  this.lastName;
    }
}
